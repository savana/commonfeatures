# README #

 In most of all iOS Applications we encounter some of the common features which need to be implemented in each and every app, with this project we can implement some of the basic  features like :

1. Check if user is running on iOS 8 or not.
2. Display default UIActivityIndicator in any view.
3. Display alert compatible in both iOS 7 and iOS 8 compatible formats.
4. Check if user's device is connected to internet or not if not then display alert message automatically.
5. Place call using URLScheme
6. Check if the Email is a valid one or not using Regex.
7. Display Web nib with back,forward and open in safari options.
8. Display Location in MKMapView with location, title and description.

License :

The MIT License (MIT)

Copyright (c) <2014> <Savana>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.