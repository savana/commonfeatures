//
//  ViewController.m
//  CommonFeatures
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}
- (void)viewDidAppear:(BOOL)animated
{
    

}
-(IBAction) load:(UIButton *)sender
{
    if (sender.tag ==1) {
        SHWebViewController *wbvc=[[SHWebViewController alloc] init];
        [wbvc  setWebUrl:@"www.google.com"];
        [self presentViewController:wbvc animated:true completion:nil];
    }else if (sender.tag == 2)
    {
        SHMapViewController *mvc=[[SHMapViewController alloc] init];
        [mvc setPlaceTitle:@"Test"];
        [mvc setPlaceDescription:@"Desc"];
        [mvc setLocation:CLLocationCoordinate2DMake(17.34, 51.55)];
        [self presentViewController:mvc animated:true completion:nil];
    }
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
