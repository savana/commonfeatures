//
//  CFMapViewController.m
//  CommonFeatures
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import "SHMapViewController.h"

@interface SHMapViewController ()

@end

@implementation SHMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpMapAndZoom];
    [self setPoint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) setUpMapAndZoom
{
    MKCoordinateRegion theRegion;
    
    CLLocationCoordinate2D userLocation=self.location;
    theRegion.center.latitude=userLocation.latitude;
    theRegion.center.longitude=userLocation.longitude;
    theRegion.span.longitudeDelta = 0.010f;
    theRegion.span.latitudeDelta = 0.010f;
    [self.mapView setRegion:theRegion animated:YES];
    [self.mapView setMapType:MKMapTypeStandard];
    self.mapView.centerCoordinate=userLocation;
}
-(void) setPoint
{
    MKPointAnnotation *pointAnnotaion=[[MKPointAnnotation alloc] init];
    if(self.placeTitle){
    pointAnnotaion.title=self.placeTitle;
    }
    if (self.placeDescription) {
    pointAnnotaion.subtitle=self.placeDescription;
    }
    pointAnnotaion.coordinate = self.location;
    [self.mapView addAnnotation:pointAnnotaion];
}

- (IBAction)bacK:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        self.mapView =nil;
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
