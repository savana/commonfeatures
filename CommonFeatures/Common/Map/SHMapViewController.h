//
//  CFMapViewController.h
//  CommonFeatures
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SHMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property  CLLocationCoordinate2D location;
@property (strong,nonatomic) NSString *placeTitle;
@property (strong,nonatomic) NSString *placeDescription;

@end
