//
//  SHActions.m
//  
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import "SHActions.h"

@implementation SHActions
+ (BOOL)isIPhone8OrHigher
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        return true;
    }
    return false;
}

+(void) displayLoading:(BOOL) display In:(UIView *)view 
{
    if (display) {
        
        UIActivityIndicatorView *loading=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [loading setBackgroundColor:[UIColor clearColor]];
        [loading setCenter:CGPointMake(view.frame.size.width/2,view.frame.size.height/2)];
        [loading setTag:123];
        [loading startAnimating];
        [view addSubview:loading];
        
    }else{
        UIActivityIndicatorView *loading=(UIActivityIndicatorView *)[view viewWithTag:123];
        [loading stopAnimating];
        [loading removeFromSuperview];
        
    }
}
+ (void)displayAlert:(NSString *)title With:(NSString *)message With:(NSString *)cancell In:(UIViewController *)controller
{
    if ([self isIPhone8OrHigher]) {
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:cancell style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:true completion:nil];
        }];
        
        [alert addAction:cancel];
        [controller presentViewController:alert animated:true completion:nil];
        
    }else{
        [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:cancell otherButtonTitles:nil] show];
    }
}
+(BOOL)  checkConnectivityAndDisplayIn:(UIViewController *) controller
{
    SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [@"google.com" UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    if (!receivedFlags || (flags == 0) )
    {
        [self displayAlert:CFNoInternetTitle With:CFNoInternetMessage With:CFDefaultAlertOk In:controller];
        return FALSE;
    } else {
        
        return  TRUE;
        
    }
    
}
+ (void)placeCallTo:(NSString *)telephoneNumber In:(UIViewController *)controller
{
    NSString *tel=@"tel:";
    tel=[tel stringByAppendingString:telephoneNumber];
    if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]]){
        
    }else{
        [self displayAlert:nil With:@"Cannot place call" With:CFDefaultAlertOk In:controller];
    }
}
+ (BOOL)validEmailField:(NSString *)email
{
    NSError *error = NULL;
    NSRegularExpression *regex = nil;
    NSUInteger numberOfMatches = 0;
    regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}"
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    numberOfMatches = [regex numberOfMatchesInString:email
                                             options:0
                                               range:NSMakeRange(0, [email length])];
    if(numberOfMatches==1){
        return TRUE;
    }else{
        return FALSE;
    }
    
}
@end
