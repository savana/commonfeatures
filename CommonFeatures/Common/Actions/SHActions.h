//
//  SHActions.h
//  SH
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>

#define CFNoInternetTitle  @"Unable to connect"
#define CFNoInternetMessage @"Please check your internet connection"
#define CFDefaultAlertOk @"Ok"
#define CFDefaultCancel @"Cancel"

@interface SHActions : NSObject
/**
 *  To check if users device is Greater than 8.0 or not
 *
 *  @return True if greater else false
 */
+(BOOL) isIPhone8OrHigher;

/**
 *  To Display UIActivityIndicatorView when theres any server fetch is taking place
 *
 *  @param display True if to be displayed, else false
 *  @param view    UIView where loader needs to be displayed
 */
+(void) displayLoading:(BOOL) display In:(UIView *)view;
/**
 *  To display alert view with parameters passed, this is a basic alert view without any delegate, completely optimised to device runnin on iOS8 or less
 *
 *  @param title      NSString title of alert
 *  @param message    NSString message of alert
 *  @param cancel     NSString cancel title @use SFRDefaultOk if need to pass @"Ok"
 *  @param controller UIViewController where alert to be displayed usually for devices running with iOS8 or higher
 */
+(void) displayAlert:(NSString *) title With:(NSString *) message With:(NSString *)cancel In:(UIViewController *)controller;
/**
 *  Check if users current device is curently connected to internet or not, if not connected then display default alert to user.
 *
 *  @param controller UIViewController where the No Internet Message to be displayed
 *
 *  @return True if connected else false
 */
+(BOOL)checkConnectivityAndDisplayIn:(UIViewController *) controller;
/**
 *  Place call to phone number passed, if user can make any calls else display alert messge that user cannot make any calls
 *
 *  @param telephoneNumber NSString number to which call need to be placed
 *  @param controller      UIViewController where alert need to be displayed if call cannot be placed.
 */
+ (void)placeCallTo:(NSString *)telephoneNumber In:(UIViewController *)controller;
/**
 *  Check if the email is a valid email using Regex
 *
 *  @param email NSString email to be checked
 *
 *  @return true if valied else false
 */
+ (BOOL)validEmailField:(NSString *)email;
@end
