//
//  SHWebViewController.h
//  CommonFeatures
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHActions.h"

@interface SHWebViewController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *constantWebView;
@property (strong,nonatomic) NSString *webUrl;
@end
