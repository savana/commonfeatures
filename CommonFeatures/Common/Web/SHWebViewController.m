//
//  CFWebViewController.m
//  CommonFeatures
//
//  Created by Savana on 10/11/2014.
//  Copyright (c) 2014 Savana. All rights reserved.
//

#import "SHWebViewController.h"

@interface SHWebViewController ()

@end

@implementation SHWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self autoSizeWebView];
    [self loadData];
    
}
-(void) autoSizeWebView
{
    CGRect tempFrame=self.constantWebView.frame;
    tempFrame.size.height=self.view.frame.size.height-91;
    self.constantWebView.frame=tempFrame;
}
-(void) loadData
{
    if (self.webUrl) {
        if (![[self.webUrl lowercaseString] containsString:@"http://"] || [[self.webUrl lowercaseString] containsString:@"https://"]) {
            NSString *temp=@"http://";
            temp=[temp stringByAppendingString:self.webUrl];
            self.webUrl=temp;
        }
        [self.constantWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webUrl]]];
        [self.constantWebView setDelegate:self];
    }
    
}
#pragma mark- UIWebView Delegates
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [SHActions displayLoading:true In:self.constantWebView];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SHActions displayLoading:false In:self.constantWebView];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [SHActions displayLoading:false In:self.constantWebView];
}

#pragma mark- button actions
- (IBAction)backTo:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)previousPage:(id)sender {
    if ([self.constantWebView canGoBack]) {
        [self.constantWebView goBack];
    }
}
- (IBAction)nextPage:(id)sender {
    if ([self.constantWebView canGoForward]) {
        [self.constantWebView goForward];
    }
}
- (IBAction)openSafari:(id)sender {
    if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.webUrl]]) {
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
